@extends('layout.master')
@section('title', 'Perpus | Kategori Buku')
@section('judul', 'Kategori Buku')
@section('content')
    <a href="/kategori/create">
        <button type="button" class="btn btn-primary">Tambah Data</button>
    </a>
    <table class="table mt-3">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Kategori Buku</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        @forelse ($kategori as $item)
        <tbody>
          <tr>
            <td>{{$item->nama}}</td>
            <td>{{$item->keterangan}}</td>
            <td>
                <form action="/kategori/{{$item->id}}" method="post">
                    @csrf
                    @method('delete')
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
                    <input type='submit' class="btn btn-danger btn-sm" value="delete">
                </form>
            </td>
          </tr>
        </tbody>
        @empty
        Tidak ada data yang ditampilkan
    @endforelse
      </table>
@endsection