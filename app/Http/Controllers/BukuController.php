<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.buku.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.buku.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'judul' => 'required',
            'penulis' => 'required',
            'tahun' => 'required',
            'penerbit' => 'required',
            'sinopsis' => 'required',
           // 'cover' => 'required|image|mimes:jpeg,png,jpg|max:2048',
           // 'file_buku' => 'required|file|mimes:pdf'
        ]);

        $coverName = time().'.'.$request->cover->extension();
        $request->cover->move(public_path('img/buku'), $coverName);

        $bukuName = time().'.'.$request->buku->extension();
        $request->buku->move(public_path('file/buku'), $bukuName);

        $buku = New Buku;
        $buku->nama = $request->judul;
        $buku->penulis = $request->penulis;
        $buku->tahun = $request->tahun;
        $buku->penerbit = $request->penerbit;
        $buku->sinopsis = $request->sinopsis;
        $buku->cover = $coverName;
        $buku->file_buku = $bukuName;
        $buku->save();

        return redirect('/buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
