@extends('layout.master')
@section('title', 'Tambah User')
@section('judul', 'Tambah User')
@section('content')
<form action="/permohonanuser/{{$permohonanuser->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="title">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$permohonanuser->nama}}" id="" placeholder="Masukkan Nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">email</label>
        <input type="text" class="form-control" name="umur" value="{{$permohonanuser->email}}" id="" placeholder="Masukkan email">
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$permohonanuser->umur}}" id="" placeholder="Masukkan Umur">
        @error('umur')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="body">Alamat</label>
        <input type="text" class="form-control" name="alamat" value="{{$permohonanuser->alamat}}" id="" placeholder="Masukkan Biodata">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">edit</button>
</form>
@endsection