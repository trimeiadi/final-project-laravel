@extends('layout.master')
@section('title', 'Perpus | Tambah Kategori')
@section('judul', 'Tambah Kategori')
@section('content')
    <form action="/kategori" method="POST">
        @csrf
        <div class="form-group">
            <label>Kategori Buku</label>
            <input type="text" class="form-control col-sm-4" name="nama" placeholder="Nama Kategori">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
            <label>Keterangan</label><br>
            <textarea class="form-control" name="keterangan" placeholder="Masukkan Keterangan buku"></textarea>
            @error('keterangan')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group row ml-3">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection