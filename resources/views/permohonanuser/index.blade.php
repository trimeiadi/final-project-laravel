@extends('layout.master')
@section('title', 'Tambah User')
@section('judul', 'Tambah User')
@section('content')
<a href="/permohonanuser/create" class="btn btn-primary mb-3">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">NO</th>
        <th scope="col">Nama</th>
        <th scope="col">email</th>
        <th scope="col">Umur</th>
        <th scope="col">alamat</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($permohonanuser as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->email}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->alamat}}</td>

                <td>
                    <form action="/permohonanuser/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        {{-- <a href="/permohonanuser/{{$value->id}}" class="btn btn-info">Show</a> --}}
                        <a href="/permohonanuser/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection