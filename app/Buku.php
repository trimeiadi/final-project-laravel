<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    Protected $table = 'buku';

    protected $fillable = ['judul', 'penulis', 'tahun', 'penerbit', 'sinopsis', 'cover', 'file_buku', 'kategori_id'];
}
