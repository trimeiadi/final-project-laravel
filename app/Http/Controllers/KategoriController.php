<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;

class KategoriController extends Controller
{
    public function index()
    {
        $kategori = Kategori::All();
        return view('admin.kategori.index', compact('kategori'));
    }

    public function create(Request $request)
    {
        return view('admin.kategori.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required',
            'keterangan' => 'required'
        ]);

        $kategori = New Kategori;
        $kategori->nama = $request->nama;
        $kategori->keterangan = $request->keterangan;
        $kategori->save();

        return redirect('/kategori');
    }

    public function edit($kategori_id)
    {
        $kategori = Kategori::where('id', $kategori_id)->first();
        return view('admin.kategori.edit', compact('kategori'));
    }

    public function update(Request $request, $kategori_id)
    {
        $request->validate([
            'nama'=>'required',
            'keterangan'=>'required'
        ]);

        $kategori = Kategori::find($kategori_id);
        $kategori->nama = $request->nama;
        $kategori->keterangan = $request->keterangan;
        $kategori->save();

        return redirect('/kategori');
    }

    public function destroy($kategori_id)
    {
        $kategori = Kategori::find($kategori_id);
        $kategori->delete();

        return redirect('/kategori');
    }
}
