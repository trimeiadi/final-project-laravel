<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    protected $table = 'profil';

    protected $fillable = ['nama', 'email', 'foto', 'umur', 'alamat', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
