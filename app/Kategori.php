<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    Protected $table = 'kategori';

    protected $fillable = ['nama', 'keterangan'];
}
