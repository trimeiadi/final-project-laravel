@extends('layout.master')
@section('title', 'Perpus | Tambah Buku')
@section('judul', 'Tambah Buku')
@section('content')
<form action="/buku" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group row">
        <label class="col-sm-3">Judul Buku</label>
        <input type="text" class="col-sm-9 form-control" name="judul" placeholder="Masukkan judul buku">
        @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group row">
        <label class="col-sm-3">Penulis</label>
        <input type="text" class="col-sm-9 form-control" name="penulis" placeholder="Masukkan penulis buku">
        @error('penulis')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group row">
        <label class="col-sm-3">Tahun Terbit</label>
        <input type="number" class="col-sm-2 form-control" name="tahun" placeholder="Tahun terbit">
        @error('tahun')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group row">
        <label class="col-sm-3">Penerbit</label>
        <input type="text" class="col-sm-6 form-control" name="penerbit" placeholder="Penerbit Buku">
        @error('penerbit')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group row">
        <label class="col-sm-3">Sinopsis Buku</label>
        <textarea name="sinopsis" class="form-control col-sm-9" placeholder="Sinopsis buku"></textarea>
        @error('sinopsis')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group row">
        <label class="col-sm-3">Kategori Buku</label>
        <select name="kategori" class="col-sm-4">
            <option>-- Pilih Kategori --</option>
            <option value="Novel">Novel</option>
            <option value="Komik">Komik</option>
        </select>
        @error('kategori')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label class="col-sm-3">Cover Buku</label>
        <input type="file" class="form-control-file col-sm-4" name="cover">
        @error('cover')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label class="col-sm-3">File Buku</label>
        <input type="file" class="form-control-file col-sm-4" name="file_buku">
        @error('file_buku')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
    <a href="/buku" type="btn" class="btn btn-danger">Batal</a> 
</form>

@endsection