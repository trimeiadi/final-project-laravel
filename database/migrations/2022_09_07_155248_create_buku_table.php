<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBukuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buku', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul', 45);
            $table->string('penulis', 45);
            $table->integer('tahun');
            $table->string('penerbit', 45);
            $table->text('sinopsis');
            $table->string('cover', 45);
            $table->string('file_buku', 45);
            $table->unsignedBigInteger('kategori_id');
            $table->foreign('kategori_id')
                ->references('id')->on('kategori');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buku');
    }
}
