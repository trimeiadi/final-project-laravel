@extends('layout.master')
@section('title', 'Perpus | Ubah Kategori')
@section('judul', 'Ubah Kategori Buku')
@section('content')
    <form action="/kategori/{{$kategori->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Kategori Buku</label>
            <input type="text" class="form-control col-sm-4" name="nama" value="{{$kategori->nama}}">
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        </div>
        <div class="form-group">
            <label>Keterangan</label><br>
            <textarea class="form-control" name="keterangan">{{$kategori->keterangan}}</textarea>
            @error('keterangan')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group row ml-3">
            <div class="col-sm-10">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
@endsection