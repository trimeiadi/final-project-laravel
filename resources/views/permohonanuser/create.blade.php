@extends('layout.master')
@section('title', 'Tambah User')
@section('judul', 'Tambah User')
@section('content')

<form action="/permohonanuser" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
      </div>
      @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <div class="form-group">
        <label for="exampleInputEmail1">Alamat Email</label>
        <input type="email" class="form-control" name="email" placeholder="EMasukkan email">
      </div>
    <div class="form-group">
        <div class="col-sm-2">
            <label for="exampleInputEmail1">Umur</label>
            <input type="number" class="form-control" name="umur" placeholder="Umur">
        </div>
    </div>
    <div class="form-group">
        <label for="exampleFormControlTextarea1">Alamat</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
      </div>
    </div>
    <div class="form-group row ml-3">
        <div class="col-sm-10">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>

@endsection