<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\permohonanuser;

class PermohonanuserController extends Controller
{
    public function create(){
        return view('permohonanuser.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
        ]);
        
        $permohonanuser = new permohonanuser;
        
        $permohonanuser->nama = $request->nama;
        $permohonanuser->email = $request->email;
        $permohonanuser->umur = $request->umur;
        $permohonanuser->alamat = $request->alamat;
        
        $permohonanuser->save();
        
        return  redirect('/permohonanuser');
        
        
    }
    
    public function index(){
        $permohonanuser = permohonanuser::all();
        return view('permohonanuser.index', compact('permohonanuser'));
        
    }
    public function show($permohonanuser_id){
        $permohonanuser = permohonanuser::where('id',$permohonanuser_id)->first();
        return view('permohonanuser.show', compact('permohonanuser'));
        
    }
    public function edit($permohonanuser_id){
        $permohonanuser = permohonanuser::where('id',$permohonanuser_id)->first();
        return view('permohonanuser.edit', compact('permohonanuser'));
        
    }

    public function update(Request $request,$permohonanuser_id){
        $request->validate([
            'nama' => 'required',
            'email' => 'required',
            'umur' => 'required',
            'alamat' => 'required'
            
        ]);

        $permohonanuser = permohonanuser::find($permohonanuser_id);

        $permohonanuser->nama = $request['nama'];
        $permohonanuser->email = $request['email'];
        $permohonanuser->umur  = $request['umur'];
        $permohonanuser->alamat = $request['alamat'];

        $permohonanuser->save();

        return redirect('/permohonanuser');


    }

    public function destroy($permohonanuser_id){
        $permohonanuser = permohonanuser::find($permohonanuser_id);
        $permohonanuser->delete();
        return redirect('/permohonanuser');

    }
}
