<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class permohonanuser extends Model
{
    protected $table = "permohonan_user";
    protected $fillable = ["nama", "email","umur","alamat"];
}
