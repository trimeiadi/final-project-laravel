<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['auth']], function () {
    
});

Route::get('/', function () {
    return view('admin.index');
});

Route::get('/pengguna', 'UserController@index');
Route::get('/pengguna/create', 'UserController@create');

Route::get('/kategori', 'KategoriController@index');
Route::get('/kategori/create', 'KategoriController@create');
Route::post('/kategori', 'KategoriController@store');
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit' );
Route::put('/kategori/{kategori_id}', 'KategoriController@update' );
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

Route::get('/permohonanuser/create','PermohonanuserController@create');
Route::post('/permohonanuser','PermohonanuserController@store');
Route::get('/permohonanuser','PermohonanuserController@index');
Route::get('/permohonanuser/{permohonanuser_id}','PermohonanuserController@show');
Route::get('/permohonanuser/{permohonanuser_id}/edit','PermohonanuserController@edit');
Route::put('/permohonanuser/{permohonanuser_id}','PermohonanuserController@update');
Route::delete('/permohonanuser/{permohonanuser_id}','PermohonanuserController@destroy');



Route::resource('buku', 'BukuController');

Auth::routes();
